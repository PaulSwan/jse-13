package ru.lebedev.tm.repository;

import ru.lebedev.tm.entity.Task;

import java.util.*;

public class TaskRepository {

    private static final Comparator<Task> NAME_COMPARATOR = new Comparator<Task>() {
        @Override
        public int compare(Task t1, Task t2) {
            if (t1.getName() != null && t2.getName() != null) {
                return t1.getName().compareTo(t2.getName());
            } else if (t1.getName() == null && t2.getName() != null) {
                return -1;
            } else if (t1.getName() != null && t2.getName() == null) {
                return 1;
            } else {
                return 0;
            }

        }

    };

    private List<Task> tasks = new ArrayList<>();
    private List<Task> userTasks = new ArrayList<>();

    public Task create(final String name) {
        final Task task = create(name, null);
        return task;
    }

    public Task create(final String name, final String description) {
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        tasks.add(task);
        return task;
    }

    public Task create(final String name, final String description, final Long userId) {
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        task.setUserId(userId);
        tasks.add(task);
        return task;
    }

    public Task update(final Long id, final String name, final String description) {
        final Task task = findById(id);
        if (Objects.isNull(task)) return null;
        task.setId(id);
        task.setName(name);
        task.setDescription(description);
        return task;

    }

    public void clear() {
        tasks.clear();
    }

    public int numberOfTasks() {
        return tasks.size();
    }

    public Task findByIndex(final int index) {
        return tasks.get(index);
    }

    public Task findByName(final String name) {
        for (final Task task : tasks) {
            if (task.getName().equals(name)) return task;
        }
        return null;

    }

    public Task removeById(final Long id) {
        final Task task = findById(id);
        if (Objects.isNull(task)) return null;
        tasks.remove(task);
        return task;

    }

    public Task removeByName(final String name) {
        final Task task = findByName(name);
        if (Objects.isNull(task)) return null;
        tasks.remove(task);
        return task;

    }

    public Task removeByIndex(final int index) {
        final Task task = findByIndex(index);
        if (Objects.isNull(task)) return null;
        tasks.remove(task);
        return task;

    }

    public List<Task> findAllByProjectId(final Long projectId) {
        final List<Task> result = new ArrayList<>();
        for (final Task task : findAll()) {
            final Long idProject = task.getProjectId();
            if (idProject == null) continue;
            if (task.getProjectId().equals(projectId)) {
                result.add(task);
            }
        }
        return result;
    }

    public Task findByProjectIdAndId(final Long projectId, final Long id) {
        if (id == null) return null;
        for (final Task task : tasks) {
            final Long idProject = task.getProjectId();
            if (idProject == null) continue;
            if (!idProject.equals(projectId)) continue;
            if (task.getId().equals(id)) return task;
        }
        return null;

    }

    public Task findByUserIdAndId(final Long userId, final Long taskId) {
        if (Objects.isNull(taskId)) return null;
        for (final Task task : tasks) {
            final Long idUser = task.getUserId();
            if (idUser == null) continue;
            if (!idUser.equals(userId)) continue;
            if (task.getId().equals(taskId)) return task;
        }
        return null;
    }

    public Task findById(final Long id) {
        for (final Task task : tasks) {
            if (task.getId().equals(id)) return task;
        }
        return null;

    }

    public List<Task> findAll() {
        return tasks;
    }

    public List<Task> findAll(final Long userId) {
        if (!Objects.isNull(userTasks) || !userTasks.isEmpty()) userTasks.clear();
        for (final Task task : tasks) {
            if (userId.equals(task.getUserId()))
                userTasks.add(task);
        }
        return userTasks;
    }

    public List<Task> findAllOrderByName() {
        List<Task> copy = new ArrayList<>(tasks);
        Collections.sort(copy, NAME_COMPARATOR);
        return copy;
    }

    public List<Task> findAllOrderByName(final Long userId) {
        if (!Objects.isNull(userTasks) || !userTasks.isEmpty()) userTasks.clear();
        for (final Task task : tasks) {
            if (userId.equals(task.getUserId()))
                userTasks.add(task);
        }
        Collections.sort(userTasks, NAME_COMPARATOR);
        return userTasks;
    }

}
