package ru.lebedev.tm.repository;

import ru.lebedev.tm.entity.Project;
import ru.lebedev.tm.entity.Task;

import java.util.*;

public class ProjectRepository {

    private static final Comparator<Project> NAME_COMPARATOR = new Comparator<Project>() {
        @Override
        public int compare(Project p1, Project p2) {
            if (p1.getName() != null && p2.getName() != null) {
                return p1.getName().compareTo(p2.getName());
            } else if (p1.getName() == null && p2.getName() != null) {
                return -1;
            } else if (p1.getName() != null && p2.getName() == null) {
                return 1;
            } else {
                return 0;
            }
        }
    };

    private List<Project> projects = new ArrayList<>();
    private List<Project> userProjects = new ArrayList<>();

    public Project create(final String name) {
        final Project project = create(name, null);
        return project;
    }

    public Project create(final String name, final String description) {
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        projects.add(project);
        return project;
    }

    public Project create(final String name, final String description, final Long userId) {
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        project.setUserId(userId);
        projects.add(project);
        return project;
    }

    public Project update(final Long id, final String name, final String description) {
        final Project project = findById(id);
        if (Objects.isNull(project)) return null;
        project.setId(id);
        project.setName(name);
        project.setDescription(description);
        return project;

    }

    public void clear() {
        projects.clear();
    }

    public int numberOfProjects() {
        return projects.size();
    }

    public Project findByIndex(final int index) {
        return projects.get(index);
    }

    public List<Project> findAll() {
        return projects;
    }

    public List<Project> findAll(final Long userId) {
        List<Project> copy = new ArrayList<>(projects);
        for (final Project project: projects) {
            if(userId.equals(project.getUserId()))
                copy.add(project);
        }
        return copy;
    }

    public List<Project> findAllOrderByName() {
        List<Project> copy = new ArrayList<>(projects);
        Collections.sort(copy,NAME_COMPARATOR);
        return copy;
    }

    public List<Project> findAllOrderByName(final Long userId) {
        List<Project> copy = new ArrayList<>(projects);
        for (final Project project: projects) {
            if(userId.equals(project.getUserId()))
                copy.add(project);
        }
        Collections.sort(copy,NAME_COMPARATOR);
        return copy;
    }

    public Project findByName(final String name) {
        for (final Project project: projects) {
            if(project.getName().equals(name)) return project;
        }
        return null;

    }

    public Project findById(final Long id) {
       for (final Project project: projects) {
            if(project.getId().equals(id)) return project;
        }
        return null;

    }

    public Project removeByIndex(final int index) {
        final Project project = findByIndex(index);
        if (Objects.isNull(project)) return null;
        projects.remove(project);
        return project;

    }

    public Project removeById(final Long id) {
        final Project project = findById(id);
        if (Objects.isNull(project)) return null;
        projects.remove(project);
        return project;

    }

    public Project removeByName(final String name) {
        final Project project = findByName(name);
        if (Objects.isNull(project)) return null;
        projects.remove(project);
        return project;

    }
    public Project findByUserIdAndId(final Long userId, final Long projectId) {
        if (Objects.isNull(projectId) || Objects.isNull(userId)) return null;
        for (final Project project: projects) {
            final Long idUser = project.getUserId();
            if(Objects.isNull(idUser)) continue;
            if(!idUser.equals(userId)) continue;
            if(project.getId().equals(projectId)) return project;
        }
        return null;
    }


}
