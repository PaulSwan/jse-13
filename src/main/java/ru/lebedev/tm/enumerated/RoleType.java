package ru.lebedev.tm.enumerated;

public enum RoleType {
    ADMIN("Administrator"),
    USER("Simple user");

    private final String displayName;


    RoleType(String displayName) {
        this.displayName = displayName;
    }


    @Override
    public String toString() {
        return displayName;
    }
}
