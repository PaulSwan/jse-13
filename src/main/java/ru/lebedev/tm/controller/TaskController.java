package ru.lebedev.tm.controller;

import ru.lebedev.tm.entity.Project;
import ru.lebedev.tm.entity.Task;
import ru.lebedev.tm.entity.User;
import ru.lebedev.tm.service.ProjectService;
import ru.lebedev.tm.service.ProjectTaskService;
import ru.lebedev.tm.service.TaskService;
import ru.lebedev.tm.service.UserService;

import java.util.List;
import java.util.Objects;

import static ru.lebedev.tm.constant.UserAlert.*;

public class TaskController extends AbstractController {

    private final TaskService taskService;
    private final ProjectService projectService;
    private final ProjectTaskService projectTaskService;
    private final UserService userService;

    public TaskController(TaskService taskService, ProjectService projectService, ProjectTaskService projectTaskService, UserService userService) {
        this.taskService = taskService;
        this.projectService = projectService;
        this.projectTaskService = projectTaskService;
        this.userService = userService;
    }

    public int createTask() {
        if (userService.noAuthUser()) {
            System.out.println(NOT_LOGGED);
            return -1;
        }
        System.out.println("[CREATE TASK]");
        System.out.println("PLEASE, ENTER TASK NAME:");
        final String name = scanner.nextLine();
        System.out.println("PLEASE, ENTER TASK DESCRIPTION:");
        final String description = scanner.nextLine();
        taskService.create(name, description, userService.getAuthUserId());
        System.out.println("[OK]");
        return 0;
    }

    public int clearTask() {
        if (userService.noAuthUser()){
            System.out.println(NOT_LOGGED);
            return -1;
        }
        if (!userService.authUserIsAdmin(userService.getAuthUserId())) {
            System.out.println(ACCESS_LEVEL_INSUFFICIENT);
            return -1;
        }
        System.out.println("[CLEAR TASK]");
        taskService.clear();
        System.out.println("[OK]");
        return 0;
    }

    public int listTask() {
        if (userService.noAuthUser()) {
            System.out.println(NOT_LOGGED);
            return -1;
        }
        System.out.println("[LIST TASK]");
        final Long authUser = userService.getAuthUserId();
        if (userService.authUserIsAdmin(authUser)){
            viewTasks(taskService.findAllOrderByName());
        } else {
            viewTasks(taskService.findAllOrderByName(authUser));
        }
        System.out.println("[OK]");
        return 0;
    }

    public void viewTasks(final List<Task> tasks){
        if (userService.noAuthUser()){
            System.out.println(NOT_LOGGED);
            return;
        }
        if(Objects.isNull(tasks) || tasks.isEmpty()) return;
        int index = 1;
        for (final Task task: tasks ){
            System.out.println(index + ". " + task.getId() + ": " + task.getName() + "| USER_ID: " + task.getUserId());
            index++;
        }
    }

    public void viewTask(final Task task) {
        if (userService.noAuthUser()){
            System.out.println(NOT_LOGGED);
            return;
        }
        if (Objects.isNull(task)) return;
        System.out.println("[VIEW TASK]");
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("USER ID: " + task.getUserId());
        if (!Objects.isNull(task.getUserId())) {
            final User user = userService.findById(task.getUserId());
            if(!Objects.isNull(user))
            System.out.println("USER ID: " + user.getLogin());
        }
        System.out.println("[OK]");

    }

    public int viewTaskByIndex() {
        if (userService.noAuthUser()) {
            System.out.println(NOT_LOGGED);
            return -1;
        }
        System.out.println("ENTER, TASK INDEX:");
        final int index = scanner.nextInt() - 1;
        final Task task = taskService.findByIndex(index);
        viewTask(task);
        return 0;

    }

    public int removeTaskByName() {
        if (userService.noAuthUser()){
            System.out.println(NOT_LOGGED);
            return -1;
        }
        if (!userService.authUserIsAdmin(userService.getAuthUserId())) {
            System.out.println(ACCESS_LEVEL_INSUFFICIENT);
            return -1;
        }
        System.out.println("[REMOVE TASK BY NAME]");
        System.out.println("PLEASE, ENTER TASK NAME:");
        final String name = scanner.nextLine();
        final Task task = taskService.removeByName(name);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
        return 0;

    }

    public int removeTaskById() {
        if (userService.noAuthUser()){
            System.out.println(NOT_LOGGED);
            return -1;
        }
        if (!userService.authUserIsAdmin(userService.getAuthUserId())) {
            System.out.println(ACCESS_LEVEL_INSUFFICIENT);
            return -1;
        }
        System.out.println("[REMOVE TASK BY ID]");
        System.out.println("PLEASE, ENTER TASK ID:");
        final long id = scanner.nextLong();
        final Task task = taskService.removeById(id);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
        return 0;

    }

    public int removeTaskByIndex() {
        if (userService.noAuthUser()){
            System.out.println(NOT_LOGGED);
            return -1;
        }
        if (!userService.authUserIsAdmin(userService.getAuthUserId())) {
            System.out.println(ACCESS_LEVEL_INSUFFICIENT);
            return -1;
        }
        System.out.println("[REMOVE TASK BY INDEX]");
        System.out.println("PLEASE, ENTER TASK INDEX:");
        final int index = scanner.nextInt() - 1;
        final Task task = taskService.removeByIndex(index);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
        return 0;

    }

    public int updateTaskByIndex() {
        if (userService.noAuthUser()) {
            System.out.println(NOT_LOGGED);
            return -1;
        }
        System.out.println("[UPDATE TASK BY INDEX]");
        System.out.println("ENTER, TASK INDEX:");
        final int index = Integer.parseInt(scanner.nextLine()) - 1;
        final Task task = taskService.findByIndex(index);
        if (Objects.isNull(task)) {
            System.out.println("[FAIL]");
            return 0;
        }
        final Long authUser = userService.getAuthUserId();
        if(userService.authUserIsAdmin(authUser) || authUser.equals(task.getUserId())) {
            System.out.println("PLEASE, ENTER TASK NAME:");
            final String name = scanner.nextLine();
            System.out.println("PLEASE, ENTER TASK DESCRIPTION:");
            final String description = scanner.nextLine();
            taskService.update(task.getId(), name, description);
            System.out.println("[OK]");
            return 0;
        } else {
            System.out.println(ACCESS_LEVEL_INSUFFICIENT);
            return -1;
        }

    }

    public int listTaskByProjectId(){
        if (userService.noAuthUser()) {
            System.out.println(NOT_LOGGED);
            return -1;
        }
        System.out.println("[LIST TASK BY PROJECT]");
        System.out.println("PLEASE, ENTER PROJECT ID:");
        final long projectId = Long.parseLong(scanner.nextLine());
        Project project = projectService.findById(projectId);
        final Long authUser = userService.getAuthUserId();
        if(userService.authUserIsAdmin(authUser) || authUser.equals(project.getUserId())) {
            final List<Task> tasks = taskService.findAllByProjectId(projectId);
            viewTasks(tasks);
            System.out.println("[OK]");
            return 0;
        } else  {
            System.out.println(ACCESS_LEVEL_INSUFFICIENT);
            return -1;
        }

    }

    public int addTaskToProjectByIds(){
        if (userService.noAuthUser()){
            System.out.println(NOT_LOGGED);
            return -1;
        }
        if (!userService.authUserIsAdmin(userService.getAuthUserId())) {
            System.out.println(ACCESS_LEVEL_INSUFFICIENT);
            return -1;
        }
        System.out.println("[ADD TASK TO PROJECT BY ID]");
        System.out.println("PLEASE, ENTER PROJECT ID:");
        final Long projectId = Long.parseLong(scanner.nextLine());
        System.out.println("PLEASE, ENTER TASK ID:");
        final long taskId = Long.parseLong(scanner.nextLine());
        if(Objects.isNull(projectTaskService.addTaskToProject(projectId, taskId))) {
            System.out.println("[FAIL]");
            return -1;
        } else {
            System.out.println("[OK]");
            return 0;
        }



    }

    public int removeTaskFromProjectByIds(){
        if (userService.noAuthUser()){
            System.out.println(NOT_LOGGED);
            return -1;
        }
        if (!userService.authUserIsAdmin(userService.getAuthUserId())) {
            System.out.println(ACCESS_LEVEL_INSUFFICIENT);
            return -1;
        }
        System.out.println("[REMOVE TASK FROM PROJECT BY ID]");
        System.out.println("PLEASE, ENTER PROJECT ID:");
        final Long projectId = Long.parseLong(scanner.nextLine());
        System.out.println("PLEASE, ENTER TASK ID:");
        final Long taskId = Long.parseLong(scanner.nextLine());
        final Task task = projectTaskService.removeTaskFromProject(projectId, taskId);
        if (Objects.isNull(task)) System.out.println("[FAIL]");
        else System.out.println("[OK]");
        return 0;
    }

 }
