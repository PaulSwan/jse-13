package ru.lebedev.tm.constant;

public class Settings {

    public static final String ADMINISTRATOR = "admin";
    public static final String SIMPLE_USER = "user";

    public static final Integer MAX_HISTORY_SIZE = 10;

}
